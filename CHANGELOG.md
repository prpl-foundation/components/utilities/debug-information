# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.2.10 - 2025-01-07(12:18:34 +0000)

### Other

- [getDebugInformation] check if there is any old archive before trying to remove them

## Release v0.2.9 - 2024-12-22(09:14:55 +0000)

### Other

- [SSH] Make getDebug working with non-root ssh user.

## Release v0.2.8 - 2024-12-05(10:15:30 +0000)

### Other

- - [getDebugInformation] Keep only the more recent archive

## Release v0.2.7 - 2024-11-28(10:04:35 +0000)

### Other

- - getDebugInformation - Still no file naming structure - integrated

## Release v0.2.6 - 2024-11-08(12:48:12 +0000)

### Other

- - Show kernel parameters in getDebug

## Release v0.2.5 - 2024-10-24(11:41:01 +0000)

### Other

- - debuginfo: add loaded kernel module list

## Release v0.2.4 - 2024-10-22(08:24:49 +0000)

### Other

- - Add persistent logs to getDebug

## Release v0.2.3 - 2024-10-16(15:43:09 +0000)

### Other

- - [Debug-information] Typo in script for dhcpv4server

## Release v0.2.2 - 2024-09-13(09:15:59 +0000)

### Other

- fix the verifyPlugin patch in getDebugInformation

## Release v0.2.1 - 2024-09-10(07:41:05 +0000)

## Release v0.2.0 - 2024-09-02(11:29:05 +0000)

### New

- getDebugInformation: add Debug.verifyPlugins() and ProxyManager.verify()

### Other

- [getDebug] Add Debug.verifyPlugins() in the getDebug

## Release v0.1.21 - 2024-07-16(08:02:02 +0000)

## Release v0.1.20 - 2024-05-29(18:31:25 +0000)

## Release v0.1.19 - 2024-03-12(14:38:22 +0000)

### Other

- [GetDebugInformation] No Oopses captured anymore after changing FaultMonitor DM

## Release v0.1.18 - 2023-12-01(10:33:44 +0000)

## Release v0.1.17 - 2023-11-07(11:17:44 +0000)

### Other

- dump containers logs

## Release v0.1.16 - 2023-09-07(10:16:48 +0000)

### Fixes

- [getDebugInformation] Not storing coredumps

## Release v0.1.15 - 2023-05-25(09:43:46 +0000)

### Fixes

- Since the addition of the fault monitor, coredumps are no longer included in the debuginfo

## Release v0.1.14 - 2023-03-24(09:37:30 +0000)

### Fixes

- Update boot_measurements script after DHCPv4 split

## Release v0.1.13 - 2022-12-19(16:13:48 +0000)

### Other

- Add missing runtime dependency on lua and lua-amx

## Release v0.1.12 - 2022-12-13(16:07:04 +0000)

### Fixes

- Improve getDebugInformation prplOs compatibility

## Release v0.1.11 - 2022-12-12(08:56:34 +0000)

### Other

- add boot measure script

## Release v0.1.10 - 2022-12-10(11:20:40 +0000)

### Other

- Add missing opensource license

## Release v0.1.9 - 2022-11-29(16:06:11 +0000)

### Other

- Opensource component

## Release v0.1.8 - 2022-10-14(17:22:52 +0000)

### Other

- [prpl] create an initial wifi debug script

## Release v0.1.7 - 2022-05-17(18:28:56 +0000)

### Fixes

- [Debug-information] change ps utility to procps-ng-ps

## Release v0.1.6 - 2022-04-22(10:43:32 +0000)

### Changes

- [debug] Add the version.txt file in the debuginformation

## Release v0.1.5 - 2022-04-06(14:20:38 +0000)

### Fixes

- Add procps as a dependency

## Release v0.1.4 - 2022-04-01(12:34:44 +0000)

### Changes

- [debug][Core file][Monitoring] Remove the debug file when zipped and update dependency

## Release v0.1.3 - 2022-03-29(10:41:24 +0000)

### Other

- [debug][Core Files] Add the core option and compression

## Release v0.1.2 - 2022-03-28(08:17:28 +0000)

### Changes

- [Monitoring] Add monitoring script for memory and process

## Release v0.1.1 - 2022-03-24(17:44:45 +0000)

### Changes

- [GetDebugInformation] Install script on prplOs

## Release v0.1.0 - 2022-03-17(08:26:43 +0000)

### New

- Install a getdebug information script in the prplOS


#!/bin/sh

LIBDEBUG=/usr/lib/debuginfo/debug_functions.sh

if [ ! -f "$LIBDEBUG" ]; then
    echo "No debug library found : $LIBDEBUG"
    exit 1
fi

. $LIBDEBUG

for SCRIPT in `ls /etc/rc.d/S*`
do 
# check at least if the debuginfo keyword is available in the start script
    grep -q debuginfo $SCRIPT
    if [ "$?" = 0 ]; then
        show_title $SCRIPT debuginfo
        show_cmd $SCRIPT debuginfo
    else 
# check at least if the status keyword is available in the start script
        grep -q status $SCRIPT
	if [ "$?" = 0 ]; then
            show_title $SCRIPT status
            show_cmd $SCRIPT status
	fi
    fi
done

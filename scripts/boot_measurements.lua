local lamx = require 'lamx'

local log_ip_interface = function(path)
  local ip_intf_path = path:gsub("IPv4Address.%d+.", "")
  local ipaddr_obj = lamx.bus.get(path, 0)                                                                          
  local ip_intf_obj = lamx.bus.get(ip_intf_path, 0)
  return "IP interface " .. ip_intf_obj[ip_intf_path].Alias .. " has ipv4address = " .. ipaddr_obj[path].IPAddress .. 
         " (" .. ip_intf_path .. "Status = " .. ip_intf_obj[ip_intf_path].Status .. ", " .. path .. "Status = " .. ipaddr_obj[path].Status .. ")"
end                      

local log_routing = function(path)
    local router_path = path:gsub("IPv4Forwarding.%d+.", "")
    local forwarding_obj = lamx.bus.get(path, 0)
    local router_obj = lamx.bus.get(router_path)
    return "Router " .. router_obj[router_path].Alias .. " has gateway ipaddress = " .. forwarding_obj[path].GatewayIPAddress
end

local log_ssh_server = function(path)
  local ssh_server_obj = lamx.bus.get(path, 0)
  return "SSH Server " .. ssh_server_obj[path].Alias .. " is " .. ssh_server_obj[path].Status
end

local log_time_client = function(path)
  local time_client_obj = lamx.bus.get(path, 0)
  return "Time Client " .. time_client_obj[path].Alias .. " is " .. time_client_obj[path].Status
end

return {
  log_file = "/tmp/amx_mon_objects.log",
  objects = {
    {
        path = "IP.Interface.*.IPv4Address.*.",
        param = "IPAddress",
        filter = "path matches '^IP.Interface.[0-9]*.IPv4Address.*$' && (contains('parameters.IPAddress') != '' || contains('parameters.Status'))",
        comment = log_ip_interface
    },
    {
        path = "SSH.Server.*.",
        param = "Status",
        value = "Running",
        comment = log_ssh_server
    },
    {
        path = "Time.Client.*.",
        param = "Status",
        filter = "contains('parameters.Status')",
        comment = log_time_client
    },
    {
        path = "Routing.Router.*.IPv4Forwarding.*.",
        param = "GatewayIPAddress",
        filter = "parameters.GatewayIPAddress != '' || parameters.GatewayIPAddress.to != ''",
        comment = log_routing
    },
    { 
        path = "Routing.Router.1.IPv4Forwarding.[Alias == 'cpe-default'].",
        param = "Status",
        value = "Enabled",
        comment = "Routing.Router.1.IPv4Forwarding.cpe-default.Status = 'Enabled'"
    },
    {
        path = "DHCPv4Client.Client.*.",
        param = "DHCPStatus",
        value = "Bound"
    },
    {
        path = "DHCPv4Server.Pool.[Alias == 'lan'].",
        param = "Status",
        value = "Enabled",
        comment = "DHCPv4Server.Pool.lan.Status = 'Enabled'"
    },
    {
        path = "DNS.Relay.",
        param = "Status",
        value = "Enabled",
        filter = "path == 'DNS.Relay.' && parameters.Status.to == 'Enabled'"
    }
  }
}

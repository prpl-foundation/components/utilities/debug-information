#!/bin/sh

case $1 in
    start|boot)
        amx_monitor_dm /bin/boot_measurements.lua & 
        ;;
    stop|shutdown)
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        cat /tmp/amx_mon_objects.log
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo]"
        ;;
esac

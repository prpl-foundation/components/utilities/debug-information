include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:

clean:

install: all
	$(INSTALL) -D -p -m 0755 scripts/debug_functions.sh $(DEST)/usr/lib/debuginfo/debug_functions.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_scripts.sh $(DEST)/usr/lib/debuginfo/debug_scripts.sh
	$(INSTALL) -D -p -m 0755 scripts/getDebugInformation $(DEST)/bin/getDebugInformation
	$(INSTALL) -D -p -m 0755 scripts/sysmon $(DEST)/bin/sysmon
	$(INSTALL) -D -p -m 0755 scripts/boot_measurements.lua $(DEST)/bin/boot_measurements.lua
	$(INSTALL) -D -p -m 0755 scripts/boot_measure.sh $(DEST)$(INITDIR)/boot_measure

package: all
	$(INSTALL) -D -p -m 0755 scripts/debug_functions.sh $(PKGDIR)/usr/lib/debuginfo/debug_functions.sh
	$(INSTALL) -D -p -m 0755 scripts/debug_scripts.sh $(PKGDIR)/usr/lib/debuginfo/debug_scripts.sh
	$(INSTALL) -D -p -m 0755 scripts/getDebugInformation $(PKGDIR)/bin/getDebugInformation
	$(INSTALL) -D -p -m 0755 scripts/sysmon $(PKGDIR)/bin/sysmon
	$(INSTALL) -D -p -m 0755 scripts/boot_measurements.lua $(PKGDIR)/bin/boot_measurements.lua
	$(INSTALL) -D -p -m 0755 scripts/boot_measure.sh $(PKGDIR)$(INITDIR)/boot_measure
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

.PHONY: all clean changelog install package
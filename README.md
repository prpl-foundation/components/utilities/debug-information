# debug-information

This repo provides a script which can be used to gather information from a openWrt/prpl build.
It is used to ease debugging and to provide information from a running system.

The repo consists of a library and a tool to help gathering debug information of other plugins.

